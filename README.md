# Podr - Server
[![Build Status](https://img.shields.io/travis/KevinTCoughlin/podr-server.svg?style=flat-square)](https://travis-ci.org/KevinTCoughlin/podr-server)
[![Coverage Status](https://img.shields.io/coveralls/KevinTCoughlin/podr-server.svg?style=flat-square)](https://coveralls.io/github/KevinTCoughlin/podr-server?branch=master)
[![npm version](https://img.shields.io/npm/v/podr-server.svg?style=flat-square)](https://www.npmjs.com/package/podr-server)
[![dependencies](https://img.shields.io/david/kevintcoughlin/podr-server.svg?style=flat-square)](https://www.npmjs.com/package/podr-server)
