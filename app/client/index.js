const request = require('request');
const config = require('./config.json');
const ENTITY = 'podcast';
const JSONStream = require('JSONStream');

module.exports = {
  getTop: (cb) => request(config.URLS.TOP, (error, response, body) => cb(JSON.parse(body))),
  search: (query, cb) => request(config.URLS.SEARCH, { term: query, entity: ENTITY }, (error, response, body) => cb(JSON.parse(body)))
}
