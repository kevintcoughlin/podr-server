const express = require('express');
const app = express();
const compression = require('compression');
const helmet = require('helmet');
const morgan = require('morgan');
const CacheControl = require("express-cache-control");
const responseTime = require('response-time')
const cache = new CacheControl().middleware;
const PodrClient = require('./client');
const VERSION = 1;
const apicache = require('apicache').middleware;

app.use(responseTime());
app.use(morgan('combined'));
app.use(compression());
app.use(helmet());
app.set('port', (process.env.PORT || 5000));

app.get('/', cache("hours", 1), apicache('5 minutes'), (req, res) => {
  res.json({ VERSION: VERSION });
});

app.get('/top', cache("hours", 1), apicache('5 minutes'), (req, res) => {
  PodrClient.getTop((data) => res.json(data));
});

app.get('/search', cache("hours", 1), apicache('5 minutes'), (req, res) => {
  PodrClient.search(req.query.query, (data) => res.json(data));
});

app.listen(app.get('port'), () => console.log('listening on', app.get('port')));

module.exports = app;
