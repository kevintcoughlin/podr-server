var request = require('supertest')
  , express = require('express')
  , assert = require('assert')
  , app = require('../index.js');

describe('GET /', () => {
  it ('respond with json', (done) => {
    request(app)
      .get('/')
      .expect(200, done);
  });
});

describe('GET /top', () => {
  it('respond with json', (done) => {
    request(app)
      .get('/top')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});

describe('GET /search', () => {
  it ('respond with json', (done) => {
    request(app)
      .get('/search')
      .expect('Content-Type', /json/)
      .expect(200, done);
  });
});
